## Лендинг для moboola.com

<a target="_blank" href="https://opensource.org/licenses/MIT" title="License: MIT">
  <img src="https://img.shields.io/badge/License-MIT-blue.svg">
</a>
<a href="#badge">
  <img alt="code style: prettier" src="https://img.shields.io/badge/code_style-prettier-ff69b4.svg">
</a>
<a target="_blank" href="http://makeapullrequest.com" title="PRs Welcome"><img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg"></a>

В качестве отправной точки рекомендуется взять структуру файлов и содержимого данного репозитория,
который содержит полностью валидный лендинг, который обрабатывает пользовательский ввод
и регистрирует юзера на сайте (в конце происходит авторизованный вход в кабинет).

Результат должен быть упакован в zip-архив по принципу 1 локаль для 1 лендинга это 1 zip-архив. \
В корне zip-архива должны находится все файлы лендинга, (т.е. вариант, когда в корне zip-архива 
находится одна директория, в которой находятся файлы - не подходит) например:

```
result.zip
         ├── images
         │   ├── bg-1-desktop.webp
         │   ├── bg-1.jpg
         │   └── logo.png
         ├── favicon.ico
         ├── app.css
         ├── index.html
         ├── app.js
         ├── vendors.js
         └── another.html
```

Образец zip-архива: <https://gitlab.com/deploy-voyagetraffic/moboola-lands-boilerplate/-/blob/master/landing-example-ru.zip>

* Обязательный состав zip-архива:
  * `index.html` - страницу входа
  * `favicon.ico`
  * `app.js`
  * `app.css`
  * html-страницы с функциональными элементами

Расширения файлов должны удовлетворять регулярному выражению: `/\.(gif|jpe?g|tiff?|png|webp|mov|avi|wmv|flv|3gp|mp4|mpg|css|js|ico|icon)/`.

### Процесс сдачи результата

Результат должен быть опубликован на странице <https://moboola.com/promo/sources/new>.

Загрузочный скрипт на данной странице валидирует контент, и результат будет зачтён, только если
содержимое zip-архива удовлетворяет требованиям, изложенным ниже. В противном случае будет показана
страница с ошибками валидации. \
Далее ссылка на опубликованный draft лендинга должна быть выслана Вами менеджеру, который ставил задачу.

Пример результата на основе файлов данного репозитория: https://moboola.com/en/promo/sources/7

### Техническое описание

##### HTML

1. Должен быть файл `index.html`.
2. Код любого html-файла не должен содержать inline javascript и css — только разметка и текст согласно локали.
3. Допускается произвольное количество `*.html` файлов.
    * Допускается вариант SPA (single page application) - когда в едином `index.html`
    весь контент меняется без перезагрузки страницы с помощью javascript (jQuery).
4. Не должно быть ошибок в консоли: ни сетевых, ни ошибок исполнения javascript.
5. Каждый файл должен содержать локаль в тэге `<html>`, например: `<html lang="ru">` или `<html lang="en">`.

##### JS

1. Весь js приложения:
    1. Должен быть внутри файла `app.js` который лежит в корне рядом с `*.html` файлами, без вложенности в директории.
    2. `app.js` должен быть подключен перед закрывающим тэгом `</body>`.
    3. `app.js` не должен быть минифицирован.
2. js-вендоры:
    1. Допускается наличие произвольного количества `*.js` либо подключение из-вне. Например: `<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>`
        * в таком случае скрипты подключаются перед закрывающим тэгом `</body>` перед подключением `app.js`.
    2. Либо их код содержится внутри `app.js` в самом начале файла.

##### CSS

1. Весь css приложения:
    1. должен быть внутри файла `app.css` который лежит в корне рядом с `*.html` файлами, без вложенности в директории.
2. css-вендоры:
    1. Допускается наличие произвольного количества `*.css` в корне рядом с `*.html` файлами, без вложенности в директории.
    2. Либо их код содержится внутри `app.css` в самом начале файла.
3. использование изображений в стилях:
    1. Использовать относительный путь к директории `images`, например: `background-image: url(images/girl.jpg);`
    2. Допускается использование base64 или svg внутри css.
4. Не должно быть кастомных шрифтов вида `url("fonts/Gilroy-Bold.woff")`.
    1. все шрифты берутся с googleapis и подключаются в `<head>`: `<link href="https://fonts.googleapis.com/css2?family=Roboto" rel="stylesheet">`.

##### Images

1. Все используемые изображения в формате gif,png,jpg хранятся в директории `images/`.
    * svg только в виде inline внутри css.
    * пример трансформации svg в поддерживаемый в css формат (javascript):
```javascript
'data:image/svg+xml;charset=utf8,' + 
'<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg"><circle cx="50" cy="50" r="50"/></svg>'
  .split("<").join("%3C").split(">").join("%3E").split("#").join("%23")
```
2. Использование изображений, которые хранятся на неких внешних хостингах, недопустимо.
3. Если JS\CSS вендор использует изображения с внешних хостингов - это допустимо.
4. Можно хранить изображения в base64 внутри css.
5. Если используется видео - то файл в формате `\.(mov|avi|wmv|flv|3gp|mp4)` не превышает 4мб и хранится внутри директории `images/`.
6. favicon лежит в корне рядом с `*.html` файлами.
7. Предпочтительный формат для больших качественных изображений - `webp`. (online-converter: <https://image.online-convert.com/convert-to-webp>)

### Html страницы с функциональными элементами

В директории `dist` содержатся файлы `static.js` и `static.css` -- это служебные файлы, в задачи
которых входят обработка функциональных элементов.

> Их необходимо подключить в html-код во время разработки лендинга.

#### Страница ввода имени

В zip-архиве в коде какого-нибудь html-файла обязательно наличие элементов с
индентификаторами `moboola_name_input` и `moboola_name_submit`, например:

```html
<input placeholder="Ваше имя" type="text" id="moboola_name_input">
<a class="button" data-href="man-screen3.html" id="moboola_name_submit">Далее</a>
```

У элемента `moboola_name_submit` должен присутствовать атрибут `data-href` с ссылкой на следующую страницу.

Пример: <https://gitlab.com/deploy-voyagetraffic/moboola-lands-boilerplate/-/blob/master/man-screen2.html>

Возможные ошибки валидации:
```
The name input «moboola_name_input» not found
The name submit «moboola_name_submit» not found
The name submit element does not have `data-href` attribute
```

#### Страница ввода email

В zip-архиве в коде какого-нибудь html-файла обязательно наличие элементов с
индентификаторами `moboola_email_input` и `moboola_email_submit`, например:

```html
<input placeholder="Ваш электронный адрес" type="email" id="moboola_email_input">
<a class="button" data-href="man-screen4.html" id="moboola_email_submit">Далее</a>
```

У элемента `moboola_email_submit` должен присутствовать атрибут `data-href` с ссылкой на следующую страницу.

Пример: <https://gitlab.com/deploy-voyagetraffic/moboola-lands-boilerplate/-/blob/master/man-screen3.html>

Возможные ошибки валидации:
```
The email input «moboola_email_input» not found
The email submit «moboola_email_submit» not found
The email submit element does not have `data-href` attribute
```

#### Страница ввода страны

В zip-архиве в коде какого-нибудь html-файла обязательно наличие элементов с
идентификаторами `moboola_country_select` и `moboola_country_submit`, например:

```html
<select data-placeholder="Ваша страна" id="moboola_country_select"></select>
<a class="button" data-href="man-screen5.html" id="moboola_country_submit">Далее</a>
```

На лендинге используется библиотека `Select2`, от вас необходимо лишь обеспечить наличие элементов
и стилизация согласно выбранному дизайну (пример стилизации в файле `app.css`).

У элемента `moboola_country_submit` должен присутствовать атрибут `data-href` с ссылкой на следующую страницу.

Пример: <https://gitlab.com/deploy-voyagetraffic/moboola-lands-boilerplate/-/blob/master/man-screen4.html>


Возможные ошибки валидации:
```
The country input «moboola_country_input» not found
The country submit «moboola_country_submit» not found
The country submit element does not have `data-href` attribute
```

#### Страница ввода даты рождения

В zip-архиве в коде какого-нибудь html-файла обязательно наличие элементов с
индентификаторами `moboola_day_select`,`moboola_month_select`,`moboola_year_select`,`moboola_birth_submit`, например:

```html
<select id="moboola_day_select" data-placeholder="День">...</select>
<select id="moboola_month_select" data-placeholder="Месяц">...</select>
<select id="moboola_year_select" data-placeholder="Год">...</select>
<a class="button" data-href="man-screen6.html" id="moboola_birth_submit">Далее</a>
```

На лендинге используется библиотека `Select2`, от вас необходимо лишь обеспечить наличие элементов
и стилизация согласно выбранному дизайну (пример стилизации в файле `app.css`).

У элемента `moboola_birth_submit` должен присутствовать атрибут `data-href` с ссылкой на следующую страницу.

Пример: <https://gitlab.com/deploy-voyagetraffic/moboola-lands-boilerplate/-/blob/master/man-screen5.html>

Возможные ошибки валидации:
```
The birth day input «moboola_day_select» not found
The birth month input «moboola_month_select» not found
The birth year input «moboola_year_select» not found
The birth submit element «moboola_birth_submit» not found
```

#### Страница ввода password

В zip-архиве в коде какого-нибудь html-файла обязательно наличие элементов с
индентификаторами `moboola_password_input` и `moboola_password_submit`, например:

```html
<input placeholder="Ваш пароль" type="password" id="moboola_password_input">
<a class="button" href="#" id="moboola_password_submit">Войти</a>
```

Пример: <https://gitlab.com/deploy-voyagetraffic/moboola-lands-boilerplate/-/blob/master/man-screen6.html>

Возможные ошибки валидации:
```
The password input «moboola_password_input» not found
The password submit element «moboola_password_submit» not found
```